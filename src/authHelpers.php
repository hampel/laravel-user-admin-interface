<?php
/**
 * 
 */

if (!function_exists('current_username'))
{
	function current_username()
	{
		return Auth::user()->username;
	}
}

if (!function_exists('current_email'))
{
	function current_email()
	{
		return Auth::user()->email;
	}
}

?>
