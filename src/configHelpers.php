<?php
/**
 * 
 */

if (!function_exists('base_layout'))
{
	function base_layout()
	{
		return Config::get('useradmin::views.base_layout');
	}
}

if (!function_exists('home_route_name'))
{
	function home_route_name()
	{
		return Config::get('useradmin::routes.home');
	}
}

?>
