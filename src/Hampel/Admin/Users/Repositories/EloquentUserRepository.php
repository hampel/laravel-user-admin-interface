<?php namespace Hampel\Admin\Users\Repositories;
/**
 * 
 */

use Illuminate\Auth\Guard as Auth;
use Illuminate\Config\Repository as Config;
use Hampel\Admin\Users\Models\User;
use Hampel\Admin\Users\Validators\UserValidator;
use Hampel\Validate\Validator;

class EloquentUserRepository implements UserRepositoryInterface
{

	/**
	 * @var \Hampel\Admin\Users\Validators\UserValidator
	 */
	protected $userValidator;

	protected $auth;

	protected $config;

	protected $validator;

	public function __construct(UserValidator $userValidator, Auth $auth, Config $config, Validator $validator)
	{
		$this->userValidator = $userValidator;
		$this->auth = $auth;
		$this->config = $config;
		$this->validator = $validator;
	}

	public function current()
	{
		return $this->auth->user();
	}

	public function create($username, $email, $password)
	{
		$user_model = $this->config->get('auth.model');

		return $user_model::create(compact('username', 'email', 'password'));
	}

	public function findByUsername($username)
	{
		$username_field = $this->config->get('user::username_field');

		return User::where($username_field, "=", $username)->first();
	}

	public function findByEmail($email)
	{
		$email_field = $this->config->get('user::email_field');

		return User::where($email_field, "=", $email)->first();
	}

	public function findById($id)
	{
		return User::find($id)->first();
	}

	public function login($username, $password, $remember = false)
	{
		$errors = $this->userValidator->validateLogin($username, $password);

		if (!$errors->isEmpty()) return $errors;

		return $this->authenticateUsername($username, $password, $remember);
	}

	protected function authenticateUsername($username, $password, $remember = false)
	{
		if ($this->validator->isEmail($username))
		{
			return $this->authenticateEmail($username, $password, $remember);
		}

		$username_field = $this->config->get('user::username_field');

		$credentials = array(
			$username_field => $username,
			'password' => $password
		);

		return $this->auth->attempt($credentials, $remember);
	}

	protected function authenticateEmail($email, $password, $remember = false)
	{
		$email_field = $this->config->get('user::email_field');

		$credentials = array(
			$email_field => $email,
			'password' => $password
		);

		return $this->auth->attempt($credentials, $remember);
	}

	public function lostPassword($email)
	{
		$errors = $this->userValidator->validateEmail($email);

		if (!$errors->isEmpty()) return $errors;

		return true;
	}

	public function passwordReset($email, $password, $password_confirmation)
	{
		$errors = $this->userValidator->validatePasswordReset($email, $password, $password_confirmation);

		if (!$errors->isEmpty()) return $errors;

		return true;
	}

	public function passwordChange($password, $new_password, $new_password_confirmation)
	{
		$errors = $this->userValidator->validatePasswordChange($password, $new_password, $new_password_confirmation);

		if (!$errors->isEmpty()) return $errors;

		return $this->changePassword($new_password);
	}

	protected function changePassword($new_password)
	{
		$user = $this->current();
		$user->password = $new_password;
		return $user->save();
	}

	public function emailChange($email, $password)
	{
		$errors = $this->userValidator->validateEmailChange($email, $password);

		if (!$errors->isEmpty()) return $errors;

		return $this->changeEmail($email);
	}

	protected function changeEmail($new_email)
	{
		$user = $this->current();
		$user->email = $new_email;
		return $user->save();
	}
}

?>
