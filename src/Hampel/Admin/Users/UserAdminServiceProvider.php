<?php namespace Hampel\Admin\Users;

use Lang, View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Factory;
use Illuminate\Validation\DatabasePresenceVerifier;
use Hampel\Admin\Users\Repositories\EloquentUserRepository;
use Hampel\Admin\Users\Validators\UserValidator;

class UserAdminServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('hampel/useradmin', 'useradmin', __DIR__ . '/../../..');

        include __DIR__ . '/../../../routes.php';
        include __DIR__ . '/../../../filters.php';

		Lang::addNamespace('useradmin', __DIR__ . '/../../../lang');
		View::addNamespace('useradmin', __DIR__ . '/../../../views');

		$this->bindRepositories();
		$this->bootCommands();
	}

	public function bindRepositories()
	{
		$this->app->bindShared('useradmin.user-validator', function($app)
		{
			return new UserValidator(
				$app['validator'],
				$app['auth']->driver(),
				$app['config'],
				$app['validate-laravel.validator']
			);
		});

		$this->app->singleton('Hampel\Admin\Users\Repositories\UserRepositoryInterface', function($app)
		{
			return new EloquentUserRepository(
				$app['useradmin.user-validator'],
				$app['auth']->driver(),
				$app['config'],
				$app['validate-laravel.validator']
			);
		});
	}

	public function bootCommands()
	{
		$this->app->bindShared('useradmin.console.config', function($app)
		{
			return new Console\ConfigCommand();
		});

		$this->app->bindShared('useradmin.console.prepare', function($app)
		{
			return new Console\PrepareCommand();
		});

		$this->app->bindShared('useradmin.console.migrate', function($app)
		{
			return new Console\MigrateCommand();
		});

		$this->app->bindShared('useradmin.console.useradd', function($app)
		{
			$users = $app->make('Hampel\Admin\Users\Repositories\UserRepositoryInterface');

			return new Console\UserAddCommand($users);
		});

		$this->app->bindShared('useradmin.console.userfind', function($app)
		{
			$users = $app->make('Hampel\Admin\Users\Repositories\UserRepositoryInterface');

			return new Console\UserFindCommand($users);
		});

		$this->app->bindShared('useradmin.console.userupdate', function($app)
		{
			$users = $app->make('Hampel\Admin\Users\Repositories\UserRepositoryInterface');

			return new Console\UserUpdateCommand($users);
		});

		$this->commands(
			'useradmin.console.config',
			'useradmin.console.prepare',
			'useradmin.console.migrate',
			'useradmin.console.useradd',
			'useradmin.console.userfind',
			'useradmin.console.userupdate'
		);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}