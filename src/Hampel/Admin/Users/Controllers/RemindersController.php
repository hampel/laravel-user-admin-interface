<?php namespace Hampel\Admin\Users\Controllers;

use App, Input, Password, Redirect, View;
use Hampel\Admin\Users\Repositories\UserRepositoryInterface;

class RemindersController extends BaseController {

	protected $users;

	protected $password_lang = array(
		Password::REMINDER_SENT => 'useradmin::auth.reset.requested',
		Password::PASSWORD_RESET => 'useradmin::auth.reset.success',
		Password::INVALID_USER => 'useradmin::auth.reset.invalid.user',
		Password::INVALID_PASSWORD => 'useradmin::auth.reset.invalid.password',
		Password::INVALID_TOKEN => 'useradmin::auth.reset.invalid.token'
	);

	public function __construct(UserRepositoryInterface $users)
	{
		parent::__construct();

		$this->users = $users;
	}

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		// Check if we already logged in
		if ($this->auth->check())
		{
			return $this->redirectProfile('useradmin::auth.reset.logoutfirst');
		}

		// Show the lost password form
		return View::make('useradmin::auth.password.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		// validate the email address
		if (($response = $this->users->lostPassword(Input::get('email'))) !== true)
		{
			// validation failed, return to the form
			return Redirect::route('useradmin.auth.lost-password')->withErrors($response)->withInput(Input::only('email'));
		}

		switch ($response = Password::remind(Input::only('email')))
		{
			case Password::INVALID_USER:
			{
				$this->alerts->error($this->password_lang[$response])->flash();

				return Redirect::route('useradmin.auth.lost-password');
			}
			case Password::REMINDER_SENT:
			{
				return $this->redirectLogin($this->password_lang[$response]);
			}
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		return View::make('useradmin::auth.password.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		// validate the reset form
		if (($response = $this->users->passwordReset(Input::get('email'), Input::get('password'), Input::get('password_confirmation'))) !== true)
		{
			// validation failed, return to the form
			return Redirect::route('useradmin.auth.reset-password', array(Input::get('token')))->withErrors($response)->withInput(Input::only('email'));
		}

		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->setPasswordAttribute($password);
			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				$this->alerts->error($this->password_lang[$response])->flash();

				return Redirect::route('useradmin.auth.reset-password', array(Input::get('token')))->withInput(Input::only('email'));

			case Password::PASSWORD_RESET:
				return $this->redirectLogin($this->password_lang[$response]);
		}
	}

	protected function redirectLogin($message)
	{
		$this->alerts->success($message)->flash();

		return Redirect::route('useradmin.auth.login');
	}

	protected function redirectProfile($message)
	{
		$this->alerts->success($message)->flash();

		return Redirect::route('useradmin.profile.show');
	}
}