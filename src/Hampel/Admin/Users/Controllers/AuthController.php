<?php namespace Hampel\Admin\Users\Controllers;
/**
 * 
 */

use View, Input, Redirect;
use Hampel\Admin\Users\Repositories\UserRepositoryInterface;

class AuthController extends BaseController {

	protected $users;

	public function __construct(UserRepositoryInterface $users)
	{
		parent::__construct();

		$this->users = $users;
	}

	/**
	 * Show Login Form
	 *
	 * @return mixed
	 */
	public function getLogin()
	{
		// Check if we already logged in
		if ($this->auth->check())
		{
			$this->alerts->success('useradmin::auth.login.already')->flash();

			// Redirect to homepage
			return Redirect::intended(route('useradmin.profile.show'));
		}

		// Show the login page
		return View::make('useradmin::auth.login');
	}

	/**
	 * Process Login form
	 *
	 * @return mixed
	 */
	public function postLogin()
	{
		$login = $this->users->login(Input::get('username'), Input::get('password'), Input::get('remember') == 'yes');

		if ($login === true)
		{
			$this->alerts->success('useradmin::auth.login.successful')->flash();

			return Redirect::intended(route('useradmin.profile.show'));
		}
		elseif ($login === false)
		{
			$this->alerts->error('useradmin::auth.login.failed')->flash();

			return Redirect::route('useradmin.auth.login')->withInput(Input::except('password'));
		}
		else
		{
			return Redirect::route('useradmin.auth.login')->withErrors($login)->withInput(Input::except('password'));
		}
	}

	/**
	 * Process log out request
	 *
	 * @return mixed
	 */
	public function getLogout()
	{
		// Log out
		$this->auth->logout();

		return $this->redirectHome('useradmin::auth.login.loggedout');
	}

	/**
	 * Show Lost Password form
	 *
	 * @return mixed
	 */
	public function getLostPassword()
	{
		// Check if we already logged in
		if ($this->auth->check())
		{
			return $this->redirectHome('useradmin::auth.reset.logoutfirst');
		}

		// Show the lost password form
		return View::make('useradmin::auth.lostpassword');
	}

	/**
	 * Process Lost Password form
	 *
	 * @return mixed
	 */
	public function postLostPassword()
	{
		$lostPassword = $this->users->lostPassword(Input::get('email'));

		if ($lostPassword === true)
		{
			return $this->users->sendReminder(Input::get('email'));
		}
		else
		{
			// Something went wrong.
			return Redirect::route('useradmin.auth.lost-password')->withErrors($lostPassword)->withInput(Input::only('email'));
		}
	}

	/**
	 * Show Password Reset form
	 *
	 * @param $token
	 *
	 * @return mixed
	 */
	public function getResetPassword($token)
	{
		// Check if we already logged in
		if ($this->auth->check())
		{
			// Redirect to homepage
			return $this->redirectHome('useradmin::auth.reset.logoutfirst');
		}

		// Show the password reset page
		return View::make('useradmin::auth.passwordreset', array('token' => $token));
	}

	/**
	 * Process Password Reset form
	 *
	 * @param $token
	 *
	 * @return mixed
	 */
	public function postResetPassword($token)
	{
		$passwordReset = $this->users->passwordReset(Input::get('email'), Input::get('password'), Input::get('password_confirmation'));

		if ($passwordReset === true)
		{
			$this->alerts->success('useradmin::auth.reset.success')->flash();

			return Redirect::route('useradmin.auth.login');
		}
		elseif ($passwordReset === false)
		{
			$this->alerts->error('useradmin::auth.reset.failed')->flash();

			return Redirect::route('useradmin.auth.login')->withInput(Input::except('password'));
		}
		else
		{
			return Redirect::route('useradmin.auth.reset-password', array('token' => Input::get('token')))->withErrors($passwordReset)->withInput(Input::only('email', 'token'));
		}
	}

	protected function redirectHome($message)
	{
		$home_route_name = $this->config->get('useradmin::routes.home', '');

		$this->alerts->success($message)->flash();

		if (empty($home_route_name))
		{
			return Redirect::to('/');
		}
		else
		{
			// Redirect to homepage
			return Redirect::route($home_route_name);
		}
	}
}


?>
