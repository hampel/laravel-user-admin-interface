<?php namespace Hampel\Admin\Users\Console;

use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MigrateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'useradmin:migrate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Perform database migrations';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$pretend = $this->option('pretend');

		$arguments = array('--package' => 'hampel/useradmin', '--database' => Config::get('useradmin::database.connection'));
		if ($pretend) $arguments['--pretend'] = '';

		$this->call('migrate', $arguments);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('pretend', null, InputOption::VALUE_NONE, 'Dump the SQL queries that would be run.', null),
		);
	}

}