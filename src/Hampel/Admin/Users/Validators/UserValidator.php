<?php namespace Hampel\Admin\Users\Validators;
/**
 * 
 */

use Illuminate\Validation\Factory;
use Illuminate\Auth\Guard as Auth;
use Illuminate\Config\Repository as Config;
use Hampel\Validate\Validator as DataValidator;

class UserValidator
{
	protected $validator;

	protected $auth;

	protected $config;

	protected $dataValidator;

	public function __construct(Factory $validator, Auth $auth, Config $config, DataValidator $dataValidator)
	{
		$this->validator = $validator;
		$this->auth = $auth;
		$this->config = $config;
		$this->dataValidator = $dataValidator;
	}

	protected function validate($data, $rules, array $translation = array())
	{
		$connection = $this->config->get('user::connection');

		$validator = $this->validator->make($data, $rules, $translation);
		$validator->getPresenceVerifier()->setConnection($connection);
		return $validator;
	}

	public function validateLogin($username, $password)
	{
		if ($this->dataValidator->isEmail($username))
		{
			$credential_field = $this->config->get('user::email_field');
		}
		else
		{
			$credential_field = $this->config->get('user::username_field');
		}

		$user_table = $this->config->get('auth.table');

		// user data for the form fields we're validating
		$user_data = array(
			'username' => $username,
			'password' => $password
		);

		$rules = array(
			'username' => array('required', "exists:{$user_table},{$credential_field}"),
			'password' => array('required', "auth:{$credential_field},{$username}")
		);

		// Validate the inputs.
		$validator = $this->validate($user_data, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validateEmail($email)
	{
		$email_field = $this->config->get('user::email_field');
		$user_table = $this->config->get('auth.table');

		// user data for the form fields we're validating
		$userdata = array(
			'email' => $email,
		);

		// Declare the rules for the form validation.
		$rules = array(
			'email'  => array('required', 'email', "exists:{$user_table},{$email_field}"),
		);

		// Validate the inputs.
		$validator = $this->validate($userdata, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validatePasswordReset($email, $password, $password_confirmation)
	{
		$user_table = $this->config->get('auth.table');

		$email_field = $this->config->get('user::email_field');

		// Get all the inputs
		$resetdata = array(
			'email' => $email,
			'password' => $password,
			'password_confirmation' => $password_confirmation,
		);


		// Declare the rules for the form validation.
		$rules = array(
			'email'  => array('required', 'email', "exists:{$user_table},{$email_field}", 'exists:password_reminders,email'),
			'password' => array('required', 'confirmed', 'min:8'),
		);

		// Validate the inputs.
		$validator = $this->validate($resetdata, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validatePasswordChange($password, $new_password, $new_password_confirmation)
	{
		// Get all the inputs
		$resetdata = array(
			'password' => $password,
			'new_password' => $new_password,
			'new_password_confirmation' => $new_password_confirmation,
		);

		$username_field = $this->config->get('user::username_field');
		$current_username = $this->auth->user()->username;

		// Declare the rules for the form validation.
		$rules = array(
			'password'  => array('required', "auth:{$username_field},{$current_username}"),
			'new_password' => array('required', 'confirmed', 'min:8'),
		);

		// Validate the inputs.
		$validator = $this->validate($resetdata, $rules);

		$validator->passes();

		return $validator->errors();
	}

	public function validateEmailChange($email, $password)
	{
		// Get all the inputs
		$email_data = array(
			'email' => $email,
			'password' => $password,
		);

		$username_field = $this->config->get('user::username_field');
		$current_username = $this->auth->user()->username;

		// Declare the rules for the form validation.
		$rules = array(
			'email' => array('required', 'email'),
			'password' => array('required', "auth:{$username_field},{$current_username}"),
		);

		// Validate the inputs.
		$validator = $this->validate($email_data, $rules);

		$validator->passes();

		return $validator->errors();
	}
}

?>
