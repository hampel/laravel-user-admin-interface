<?php
/**
 * 
 */

if (!function_exists('auth_translation'))
{
	function auth_translation($part = "")
	{
		return Lang::get('validate-laravel-auth::validation' . ".{$part}");
	}
}

if (!function_exists('validation_translation'))
{
	function validation_translation($part = "")
	{
		return Lang::get('validate-laravel::validation' . ".{$part}");
	}
}

?>
