<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Base Layout Name
	|--------------------------------------------------------------------------
	|
	| The name of the base layout for views
	|
	| The base layout will need to include a @yield('body') section, and should
	| also include:
	|
	| @include('alerts::alerts')
	|
	| Default value: 'useradmin::layouts.base'
	|
	*/

	'base_layout' => 'useradmin::layouts.base',

);

?>
