<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Home route name
	|--------------------------------------------------------------------------
	|
	| The named route used for the "home" page of the application
	|
	| Used when redirecting from operations such as logging out.
	|
	| If there is no named route for home, leave this blank and controllers will
	| redirect to '/' instead
	|
	| Default value: ''
	|
	*/

	'home' => '',

	/*
	|--------------------------------------------------------------------------
	| Admin Group Rules
	|--------------------------------------------------------------------------
	|
	| Rules for grouping the admin area routes, for adding a prefix or
	| subdomain
	|
	| Default value: array()
	|
	*/

	'admin_group_rules' => array(
		// 'prefix' => 'admin',
		// 'domain' => 'admin.site.com'
	),

	/*
	|--------------------------------------------------------------------------
	| Profile Group Rules
	|--------------------------------------------------------------------------
	|
	| Rules for grouping the user profile area routes, for adding a prefix or
	| subdomain
	|
	| Default value: array()
	|
	*/

	'profile_group_rules' => array(
		// 'prefix' => 'profile',
		// 'domain' => 'profiless.site.com'
	),

);

?>
