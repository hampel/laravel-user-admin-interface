@extends(base_layout())

{{-- Show Profile --}}

@section('body')
<div class="row">
	<div class="large-12 columns">

		<h2>Profile</h2>

		<p>You are logged in as {{ current_username() }}</p>

		<p>{{ link_to_route('useradmin.profile.change-password', 'Change password') }}</p>
		<p>{{ link_to_route('useradmin.profile.change-email', 'Change email') }}</p>
		<p>{{ link_to_route('useradmin.auth.logout', 'Logout') }}</p>

	</div>
</div>
@stop
