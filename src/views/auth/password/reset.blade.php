@extends(base_layout())

{{-- Login page layout --}}

@section('body')
<div class="row">
	<div class="large-12 columns">

		<h2>Password Reset</h2>
		{{ Form::open(array('route' => array('useradmin.auth.reset-password', $token))) }}
		{{ Form::hidden('token', $token) }}

		<p>You may choose a new password here.</p>

		<!-- Email -->
		<div class="row collapse {{{ $errors->has('email') ? 'error' : '' }}}">
			<div class="large-2 columns">
				<span class="prefix radius">{{ Form::label('email', 'Email') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::text('email', Input::old('email')) }}
				{{ $errors->first('email', '<small class="error">:message</small>') }}
			</div>
			<div class="large-7 columns">&nbsp;</div>
		</div>

		<!-- Password -->
		<div class="row collapse {{{ $errors->has('password') ? 'error' : '' }}}">
			<div class="large-2 columns">
				<span class="prefix radius">{{ Form::label('password', 'New Password') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::password('password') }}
				{{ $errors->first('password', '<small class="error">:message</small>') }}
			</div>
			<div class="large-7 columns">&nbsp;</div>
		</div>

		<!-- Password -->
		<div class="row collapse {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
			<div class="large-2 columns">
				<span class="prefix radius">{{ Form::label('password_confirmation', 'Confirm Password') }}</span>
			</div>

			<div class="large-3 columns">
				{{ Form::password('password_confirmation') }}
				{{ $errors->first('password_confirmation', '<small class="error">:message</small>') }}
			</div>
			<div class="large-7 columns">&nbsp;</div>
		</div>

		<!-- Login button -->
		<div class="control-group">
			<div class="controls">
				{{ Form::submit('Reset Password', array('class' => 'button medium radius')) }}
			</div>
		</div>

		{{ Form::close() }}
	</div>
</div>
@stop
