@extends(base_layout())

{{-- Login page layout --}}

@section('body')
	<div class="row">
		<div class="large-12 columns">

			<h2>Login into your account</h2>
			{{ Form::open(array('route' => 'useradmin.auth.login')) }}
			{{ Form::token(); }}

			<!-- username -->
			<div class="row collapse {{{ $errors->has('username') ? 'error' : '' }}}">
				<div class="large-1 columns">
					<span class="prefix radius">{{ Form::label('username', 'Username') }}</span>
				</div>
				<div class="large-3 columns">
					{{ Form::text('username', Input::old('username')) }}
					{{ $errors->first('username', '<small class="error">:message</small>') }}
				</div>
				<div class="large-8 columns">&nbsp;</div>
			</div>

			<!-- Password -->
			<div class="row collapse {{{ $errors->has('password') ? 'error' : '' }}}">
				<div class="large-1 columns">
					<span class="prefix radius">{{ Form::label('password', 'Password') }}</span>
				</div>

				<div class="large-3 columns">
					{{ Form::password('password') }}
					{{ $errors->first('password', '<small class="error">:message</small>') }}
				</div>
				<div class="large-8 columns">&nbsp;</div>
			</div>

			<!-- Remember Me -->
			<div class="row collapse {{{ $errors->has('remember') ? 'error' : '' }}}">
				<div class="large-4 columns">
					{{ Form::checkbox('remember', 'yes'); }} {{ Form::label('remember', 'Remember Me', $attributes = array("class" => "inline", "style" => "display: inline;")) }}
				</div>
				<div class="large-8 columns">&nbsp;</div>
			</div>

			<!-- Login button -->
			<div class="control-group">
				<div class="controls">
					{{ Form::submit('Login', array('class' => 'button medium radius')) }} &raquo; <small>{{ link_to_route('useradmin.auth.lost-password', 'Forgotten your password?') }}</small>
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
@stop
