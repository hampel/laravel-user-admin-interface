<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Profile Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'password' => array(
		'success' => 'Password change successful',
		'failed' => 'Password change failed',
	),

	'email' => array(
		'success' => 'Email change successful',
		'failed' => 'Email change failed',
	)
);