CHANGELOG
=========

1.3.2 (2014-07-09)
------------------

* fixes for EloquentUserRepository - when sending credentials for authentication, password must have an array key of 'password', regardless of password column name
* remove redundant use statement from routes.php

1.3.1 (2014-07-08)
------------------

* set connection on presence verifier for validator
* fixes for UserValidator - validation array keys relate to form fields, not database fields

1.3.0 (2014-07-06)
------------------

* refactoring
* stopped using helper functions in controllers for better readability
* stopped using helper functions in user repository for better readability
* tidy up service provider, swap to using bindShared
* stopped using helper functions in userValidator for better readability

1.2.0 (2014-07-04)
------------------

* implemented hampel/user Configurable User Model

1.1.1 (2014-07-04)
------------------

* fixed URLs for issues and support in composer.json
* updated framework dependencies to ~4.1 ... no point specifying ~4.0 if we have a minimum of 4.1.29 on one package

1.1.0 (2014-07-03)
------------------

* major rewrite to take advantage of password reminder changes in framework v4.1.29
* less restrictive versioning for dependencies
* remove obsolete tests
* added migration for remember_token in user table
* implemented hampel/alerts package for session alerts

1.0.5 (2013-10-21)
------------------

* added `class="error"` to error messages for Foundation
* moved lang helper functions to separate file
* implemented passing of translation array to validator via Factory::extend method
* minimum version of illuminate/validation set to 4.0.9 to use additional parameter on Factory::extend

1.0.4 (2013-10-20)
------------------

* separated user validation functions into separate class, injected into user repository in service provider

1.0.3 (2013-10-17)
------------------

* added custom validator to package so we can use a separate database connection for presence verification
* fixed bug in EloquentUserRepository where emails were not accepted for login

1.0.2 (2013-10-17)
------------------

* removed database.users_table config option in favour of using core auth.table value instead
* changed migration to use hard-coded table name, implementer will need to provide their own migrations if want to use
  a different name
* cleaned up Service Provider code
* updated User model to use configuration options for table and fillable
* update README with installation information

1.0.1 (2013-10-16)
------------------

* added hampel/validate-laravel package to requirements in composer.json

1.0.0 (2013-10-16)
------------------

* initial release
